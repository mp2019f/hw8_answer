package kr.ac.mju.mp2019f.contactsandsms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button Contant;
    private Button Sns;

    int what = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Contant = findViewById(R.id.button);
        Sns = findViewById(R.id.button2);

        Contant.setOnClickListener(this);
        Sns.setOnClickListener(this);




    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.button:
                what = 0;

                Intent intent = new Intent(getApplicationContext(), Second.class);
                intent.putExtra("what",what);

                startActivity(intent);

                break;
            case R.id.button2:
                what = 1;

                Intent intent1 = new Intent(getApplicationContext(), Second.class);
                intent1.putExtra("what",what);

                startActivity(intent1);
                break;
        }



    }
}
