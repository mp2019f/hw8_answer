package kr.ac.mju.mp2019f.contactsandsms;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.net.URI;
import java.nio.channels.AsynchronousChannel;

public class Second extends AppCompatActivity {
    final int REQUEST_CODE_READ_CONTACTS = 1;
    private ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        int what = intent.getIntExtra("what",0);

        list = findViewById(R.id.list);

        if(what == 0){
            if (ContextCompat.checkSelfPermission(Second.this, Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Second.this,
                        new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_CODE_READ_CONTACTS);
        } else
            getContacts();
        }
        if(what == 1){
            if (ContextCompat.checkSelfPermission(Second.this, Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Second.this,
                        new String[]{Manifest.permission.READ_SMS}, REQUEST_CODE_READ_CONTACTS);
            }
            else
                SMSContacts();

        }
    }

    private void SMSContacts(){

        Uri uri = Uri.parse("content://sms/inbox");

        String[] reqCols = new String[]{"_id", "address", "body"};
        ContentResolver cr = getContentResolver();

        Cursor c = cr.query(uri, reqCols, null, null, null);
        String[] contactsColumns = {
                "address",
                "body"
        };

        int[] contactsListItems = {
                R.id.text01,
                R.id.text02
        };

        SimpleCursorAdapter adapter1 = new SimpleCursorAdapter(this,
                R.layout.listview,
                c,
                contactsColumns,
                contactsListItems,
                0);


       adapter1.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
           @Override
           public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
               if(columnIndex == 1){
                   String number = cursor.getString(1);
                   String name = name(number);
                   TextView text = (TextView) view;
                   text.setText(name);
                   return true;
               }
               return false;
           }
       });
        list.setAdapter(adapter1);
           }



    private String name(String number){
        Uri uri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String name = " ";

        ContentResolver contentResolver = getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[] {
                        BaseColumns._ID, ContactsContract.PhoneLookup.DISPLAY_NAME },
                null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                name = contactLookup.getString(contactLookup
                        .getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }
        if(name == " ")
            return number;
        return name;
    }

    private void getContacts() {

        String [] projection = {
                ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER

        };
         Cursor c = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection,
                null,
                null,
                null);



        String[] contactsColumns = {
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        int[] contactsListItems = {
                R.id.text01,
                R.id.text02
        };

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.listview,
                c,
                contactsColumns,
                contactsListItems,
                0);



        list.setAdapter(adapter);



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getContacts();
            } else {
                Toast.makeText(getApplicationContext(), "READ_CONTACTS 접근 권한이 필요합니다", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
